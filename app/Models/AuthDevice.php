<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthDevice extends Model
{
    use HasFactory;

    protected $table = "user_device";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'device_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
