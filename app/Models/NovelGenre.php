<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NovelGenre extends Model
{
    use HasFactory;

    protected $table = "novel_genres";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    protected $hidden = ['pivot'];

    public $timestamps = false;
    
    public function novels()
    {
        return $this->belongsToMany(Novel::class, "novel_genres_relation", "genre_id", "novel_id");
    }
}
