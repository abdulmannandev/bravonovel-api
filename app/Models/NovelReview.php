<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NovelReview extends Model
{
    use HasFactory;

    protected $table = "novel_review";
    protected $fillable = [
        'novel_id','user_id','text','rate','like_count',
    ];
    public function novel()
    {
        return $this->belongsTo(Novel::class);
    }
    public function novel_review_like()
    {
        return $this->hasMany(NovelReviewLike::class,'review_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
