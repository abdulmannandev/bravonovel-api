<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Core\Auth\User as BaseUser;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends BaseUser implements MustVerifyEmail
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function devices()
    {
        return $this->hasMany(AuthDevice::class);
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'state' => "verified",
        ])->save();
    }

    /**
     * Check if user verified.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        return $this->state == "verified";
    }

    /**
     * Check if user inactive.
     *
     * @return bool
     */
    public function isBanned()
    {
        return $this->state == "banned";
    }

    public function novels()
    {
        return $this->hasMany(Novel::class);
    }

    public function novelChapters()
    {
        return $this->hasMany(NovelChapter::class);
    }

    public function novelChaptersHistory()
    {
        return $this->hasMany(NovelChapterHistory::class);
    }

    public function gifts()
    {
        return $this->hasMany(Gift::class);
    }
}
