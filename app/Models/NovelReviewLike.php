<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NovelReviewLike extends Model
{
    use HasFactory;

    protected $table = "novel_review_like";

    protected $fillable = [
        'review_id','user_id','like_at',
    ];

    public $timestamps = false;

    public function novel()
    {
        return $this->belongsTo(Novel::class);
    }

    public function novel_review()
    {
        return $this->belongsTo(NovelReview::class,'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
