<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Novel extends Model
{
    use HasFactory;

    protected $table = 'novel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'keyword', 'synopsis', 'image', 'user_id', 'state', 'published', 'watch_count'
    ];

    protected $hidden = ['pivot'];

    public function novelGenres()
    {
        return $this->belongsToMany(NovelGenre::class, "novel_genres_relation", "novel_id", "genre_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function novelChapters()
    {
        return $this->hasMany(NovelChapter::class);
    }

    public function gifts()
    {
        return $this->hasMany(Gift::class);
    }
}
