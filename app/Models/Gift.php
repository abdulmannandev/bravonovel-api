<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    use HasFactory;

    protected $table = 'gift';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'img', 'price'
    ];

    protected $hidden = ['pivot'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function novel()
    {
        return $this->hasMany(Novel::class);
    }
}
