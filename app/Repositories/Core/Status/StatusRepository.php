<?php


namespace App\Repositories\Core\Status;


use App\Exceptions\GeneralException;
use App\Models\Core\Status;
use App\Repositories\Core\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class StatusRepository extends BaseRepository
{
    public function __construct(Status $status)
    {
        $this->model = $status;
    }

    /**
     * @param string $type
     * @return mixed | Collection
     * @throws \Exception
     */
    public function statuses($type = '')
    {
        return $this->get()->when($type, function (Collection $collection) use ($type) {
            return $collection->filter(function ($status) use ($type) {
                return $status->type == $type;
            });
        })->values();
    }

    public function get() : Collection
    {
        return cache()->rememberForever('statuses', function () {
            return DB::table('statuses')
                ->get(['id', 'name', 'type', 'class'])
                ->map(function ($status) {
                    $status->translated_name = trans('default.'.$status->name);
                    return $status;
                });
        });
    }

    public function __call($name, $arguments)
    {
        $status = preg_split('/(^[^A-Z]+|[A-Z][^A-Z]+)/', $name,-1, PREG_SPLIT_NO_EMPTY |
            PREG_SPLIT_DELIM_CAPTURE );

        [$type] = $status;

        if ( ($count = count($status)) > 1) {
            $q = 'status';
            for ($i = 1; $i <= $count - 1; $i++) $q .=  '_' . strtolower($status[$i]);
            return $this->getStatusId($type, $q);
        }

        $statuses = $this->statuses($type);
        if ($arguments) {
            return $statuses->whereIn('name', $arguments)
                ->pluck('name', 'id')
                ->toArray();
        }
        return $statuses;

    }

    public function getStatusId($type, $name)
    {
        $row = $this->statuses($type)
            ->firstWhere('name', $name);

        return $row ? $row->id : new GeneralException('Status Not Found');
    }
}
