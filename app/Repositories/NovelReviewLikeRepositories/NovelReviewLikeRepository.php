<?php

namespace App\Repositories\NovelReviewLikeRepositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\NovelReviewRepositories\NovelReviewRepository;

class NovelReviewLikeRepository extends Repository
{
    protected $novelReviewRepository;

    public function __construct(NovelReviewRepository $novelReviewRepository, App $app) {
        $this->novelReviewRepository = $novelReviewRepository;

        parent::__construct($app);
    }
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model() {
        return 'App\Models\NovelReviewLike';
    }

    /**
     * @param  int  $novel
     * @return \Illuminate\Support\Collection
     */
    public function getAll($novelId = null)
    {
        $query = $this->scopeQuery(function($query) use ($novelId) {

            $query = $query->with('user');
            foreach (request()->only(['page', 'limit', 'pagination', 'sort', 'order']) as $input => $value) {
                $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if ($novelId) {
                $query = $query->whereHas('novel_review', function ($qb) use ($novelId) {
                    $qb->where('id', $novelId);
                });
            }

            if (request()->has('search'))
            $query->where('title', 'like', '%' . urldecode(request()->input('search')) . '%');

            if ($sort = request()->input('sort')) {
                $query = $query->orderBy($sort, request()->input('order') ?? 'desc');
            } else {
                $query = $query->orderBy('id', 'asc');
            }

            return $query;
        })->get();

        return $query;
    }

    public function store(array $request)
    {
        $novelReviewRepository = $this->novelReviewRepository->where('id', $request['review_id']);
        if ($novelReviewRepository->exists()) {
            if (!$this->where(['review_id' => $request['review_id'], 'user_id' => $request['user_id']])->exists())
            {
                $this->create($request);
            }
            else
                $this->where(['review_id' => $request['review_id'], 'user_id' => $request['user_id']])->delete();
            
            
            $totalReviewLikes = $this->where('review_id', $request['review_id'])->count();

            $novelReviewRepository->update([
                'like_count' => $totalReviewLikes
            ]);
        } else {
            throw (new ModelNotFoundException)->setModel(
                get_class($this->model)
            );
        }
    }
}
