<?php

namespace App\Repositories\NovelReviewRepositories;

use App\Repositories\Repository;

class NovelReviewRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model() {
        return 'App\Models\NovelReview';
    }

    /**
     * @param  int  $novel
     * @return \Illuminate\Support\Collection
     */
    public function getAll($novelId = null)
    {
        $query = $this->scopeQuery(function($query) use ($novelId) {

            $query = $query->with('user');
            foreach (request()->only(['page', 'limit', 'pagination', 'sort', 'order']) as $input => $value) {
                $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if ($novelId) {
                $query = $query->whereHas('novel', function ($qb) use ($novelId) {
                    $qb->where('id', $novelId);
                });
            }

            if (request()->has('search'))
            $query->where('title', 'like', '%' . urldecode(request()->input('search')) . '%');

            if ($sort = request()->input('sort')) {
                $query = $query->orderBy($sort, request()->input('order') ?? 'desc');
            } else {
                $query = $query->orderBy('id', 'asc');
            }

            return $query;
        })->get();

        return $query;
    }
}
