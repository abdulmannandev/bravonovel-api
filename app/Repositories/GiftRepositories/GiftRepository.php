<?php

namespace App\Repositories\GiftRepositories;

use Illuminate\Http\Request;
use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class GiftRepository extends Repository
{
    
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Gift';
    }

    public function createUserGifts(array $data)
    {
        if (app('App\Models\User')->where('id', $data['to_user_id'])->exists()) {
            $gift = app($this->model())->where('id', $data['gift_id'])->first();
            if ($gift)
                DB::table('gift_user')->insert($data);
            else
                return false;
        } else {
            return false;
        }
        return true;
    }
}
