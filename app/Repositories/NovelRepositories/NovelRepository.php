<?php

namespace App\Repositories\NovelRepositories;

use App\Repositories\Repository;

class NovelRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model() {
        return 'App\Models\Novel';
    }

    /**
     * @param  int  $genreId
     * @return \Illuminate\Support\Collection
     */
    public function getAll($genreId = null)
    {
        $query = $this->scopeQuery(function($query) use ($genreId) {
            $query = $query->with('user');
            foreach (request()->only(['page', 'limit', 'pagination', 'sort', 'order']) as $input => $value) {
                $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if ($genreId) {
                $query = $query->whereHas('novelGenres', function ($qb) use ($genreId) {
                    $qb->where('id', $genreId);
                });
            }

            if (request()->has('search'))
                $query->where('title', 'like', '%' . urldecode(request()->input('search')) . '%');

            if ($sort = request()->input('sort')) {
                $query = $query->orderBy($sort, request()->input('order') ?? 'desc');
            } else {
                $query = $query->orderBy('id', 'desc');
            }

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('limit') ?? 10);
        } else {
            $results = $query->get();
        }

        return $results;
    }
}
