<?php

namespace App\Repositories\UserRepositories;


use App\Models\User;
use App\Models\Core\Status;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model() {
        return User::class;
    }

    public function createUser(Request $request)
    {
        $user = $this->create([
            'name' => $request->name,
            'email' => $request->email,
            'new_user' => true,
            'password' => Hash::make($request->password),
            'status_id' => Status::findByNameAndType('status_active', 'user')->id,
        ]);

        return $user->where('id', $user->id)->first();
    }
}
