<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\GiftRepositories\GiftRepository;
use App\Resources\Gift\GiftResource;
use Illuminate\Support\Facades\DB;

class GiftController extends Controller
{
    private $giftRepository;

    /**
     * CrudController constructor.
     * @param CrudService $service
     * @param CrudFilter $filter
     */
    public function __construct(GiftRepository $giftRepository)
    {
        $this->middleware('auth:api');

        $this->giftRepository = $giftRepository;
    }

    public function index()
    {
        $user = request()->user();

        $query = $this->giftRepository->scopeQuery(function($query) use ($user) {
            foreach (request()->except(['page', 'limit', 'pagination', 'sort', 'order']) as $input => $value) {
                $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            
            $query = $query->distinct()
                        ->whereExists(function($q) use ($user) {
                            $q->select(DB::raw(1))
                            ->from('gift_user')
                            ->leftJoin('users', 'users.id', '=', 'gift_user.to_user_id')
                            ->where('gift_user.to_user_id', $user->id);
                        });

            if ($sort = request()->input('sort')) {
                $query = $query->orderBy($sort, request()->input('order') ?? 'desc');
            } else {
                $query = $query->orderBy('id', 'desc');
            }

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('limit') ?? 10);
        } else {
            $results = $query->get();
        }

        return GiftResource::collection($results);
    }

    public function store(Request $request)
    {
        $request->validate([
            'gift_id' => ['required'],
            'novel_id' => ['required']
        ]);

        $user = $request->user();
        $request->request->add(['from_user_id' => $user->id]);

        $response = $this->giftRepository->createUserGifts($request->all());

        if ($response)
            return response()->json([
                'message' => 'Gift Added'
            ]);
        else
            return response()->json([
                'message' => 'Gift not Added some paremeters missing.'
            ], 422);
    }
}
