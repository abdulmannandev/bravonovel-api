<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\NovelChapterRepositories\NovelChapterRepository;
use App\Resources\NovelChapter\NovelChapterResouce;
use Illuminate\Http\Request;

class NovelChaptersController extends Controller
{
    protected $novelChapterRepository;

    public function __construct(NovelChapterRepository $novelChapterRepository)
    {
        $this->novelChapterRepository = $novelChapterRepository;
        $this->middleware('auth:api');
    }

    public function getNovelChapters()
    {
        $this->validate(request(), [
            "novel_id" => "required|numeric"
        ]);

        return NovelChapterResouce::collection($this->novelChapterRepository->getAll(request()->input('novel_id')));
    }


   
}
