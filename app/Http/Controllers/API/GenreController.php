<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    protected $repository;

    public function __construct() {
        $this->repository = app(request('_config')['repository']);
    }
}
