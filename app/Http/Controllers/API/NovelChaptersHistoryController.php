<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\NovelChapterHistoryRepositories\NovelChapterHistoryRepository;
use App\Resources\NovelChapterHistory\NovelChapterHistoryResouce;
use Illuminate\Http\Request;

class NovelChaptersHistoryController extends Controller
{
    protected $novelChapterHistoryRepository;

    public function __construct(NovelChapterHistoryRepository $novelChapterHistoryRepository)
    {
        
        $this->novelChapterHistoryRepository = $novelChapterHistoryRepository;
    }


    public function getNovelChaptersHistory()
    {
        $this->validate(request(), [
            "novel_id" => "required|numeric"
        ]);

        return NovelChapterHistoryResouce::collection($this->novelChapterHistoryRepository->getAll(request()->input('novel_id')));
    }
}
