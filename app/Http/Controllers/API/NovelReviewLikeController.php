<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\NovelReviewLikeRepositories\NovelReviewLikeRepository;
use App\Resources\NovelReviewLike\NovelReviewLikeResouce;
use Illuminate\Http\Request;
use Carbon\Carbon;

class NovelReviewLikeController extends Controller
{
    protected $novelreviewlikeRepository;

    public function __construct(NovelReviewLikeRepository $novelreviewlikeRepository)
    {
        $this->middleware('auth:api');
        
        $this->novelreviewlikeRepository = $novelreviewlikeRepository;
    }


    public function getNovelReviewLike()
    {
        $this->validate(request(), [
            "review_id" => "required|numeric"
        ]);

        return NovelReviewLikeResouce::collection($this->novelreviewlikeRepository->getAll(request()->input('review_id')));
    }

    public function StoreNovelReviewLike(Request $request)
    {
        #code...
        $this->validate(request(), [
            "review_id" => "required|numeric"
        ]);

        $user_id = $request->user()->id;
        $request->request->add(['user_id' => $user_id, "like_at" => Carbon::now()]);

        $this->novelreviewlikeRepository->store($request->all());
        return response()->json([ 'message' => "Review like added successfully" ]);
    }
}
