<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\NovelReviewRepositories\NovelReviewRepository;
use App\Resources\NovelReview\NovelReviewResouce;
use Illuminate\Http\Request;

class NovelReviewController extends Controller
{
    protected $novelreviewRepository;

    public function __construct(NovelReviewRepository $novelreviewRepository)
    {
        $this->middleware('auth:api');

        $this->novelreviewRepository = $novelreviewRepository;
    }


    public function getNovelReview()
    {
        $this->validate(request(), [
            "novel_id" => "required|numeric"
        ]);

        return NovelReviewResouce::collection($this->novelreviewRepository->getAll(request()->input('novel_id')));
    }

    public function StoreNovelReview(Request $request)
    {
        #code...
        $this->validate(request(), [
            "novel_id" => "required|numeric",
            "text" => "required",
            "rate" => "required"
        ]);

        $user_id = $request->user()->id;
        $request->request->add(['user_id' => $user_id]);
        
        $this->novelreviewRepository->create($request->all());

        return response()->json([ 'message' => "Review added successfully" ]);
    }
}
