<?php

namespace App\Http\Controllers\API\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Process forgot password request and send reset token.
     *
     * @param  Request $request
     * @return \App\User
     */
    function forgot(Request $request)
    {
        try {
            $request->validate([
                'email' => ['required', 'email'],
            ]);

            $user = User::where('email', $request->email)->first();

            if (! $user) {
                throw ValidationException::withMessages([
                    'email' => ['The provided credentials are incorrect.'],
                ]);
            }
            
            if (!$user->isActive()) {
                return response()->json([
                    'message' => 'user is inactive.'
                ], 403);
            } else if ($user->isBanned()) {
                return response()->json([
                    'message' => 'user is banned.'
                ], 403);
            }

            $response = Password::broker()->sendResetLink([
                'email' => $user->email
            ]);
            
            return response()->json(['message' => 'Reset link sent to your email.'], 200);

        } catch (Exception $error) {
            return response()->json([
                'message' => $error,
            ], 401);
        }
    }
}
