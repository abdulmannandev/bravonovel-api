<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Models\AuthDevice;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller as Controller;
use App\Repositories\UserRepositories\UserRepository;

class RegisterController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function register(Request $request)
    {
        try {
            $request->validate([
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:user'],
                'password' => ['required', 'string', 'min:8'],
                'device_id' => ['required', 'string'],
            ]);

            $user = $this->userRepository->createUser($request);
            $user->sendEmailVerificationNotification();
            $user->devices()->save(new AuthDevice([
                'device_id' => $request->get('device_id')
            ]));

            return response()->json([
                "token" => $user->createToken($request->get('device_id'))->accessToken,
                "user" => $user,
                'message' => 'user register successfully.'
            ]);
        }    
        catch (\Exception $error) {
            return response()->json([
                'message' => $error,
            ], 401);
        }
    }
}
