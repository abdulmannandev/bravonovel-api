<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\AuthDevice;
use App\Http\Controllers\Controller as Controller;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user();

            if ($request->has('device_id')) {
                $user->devices()->save(new AuthDevice([
                    'device_id' => $request->get('device_id')
                ]));
                
                if (!$user->isActive()) {
                    return response()->json([
                        'message' => 'user is inactive.'
                    ], 403);
                } else if ($user->isBanned()) {
                    return response()->json([
                        'message' => 'user is banned.'
                    ], 403);
                }
    
                return response()->json([
                    "token" => $user->createToken($request->get('device_id'))->accessToken,
                    "user" => $user,
                    'message' => 'user login successfully.'
                ]);
            } else {
                return response()->json([
                    'message' => 'device id is required'
                ]);
            }
        } 
        else{ 
            return response()->json([
                'message' => 'Oops! You have typed incorrect Email or Password.'
            ], 401);
        }
    }
}
