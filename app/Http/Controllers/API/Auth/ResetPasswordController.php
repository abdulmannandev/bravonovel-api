<?php

namespace App\Http\Controllers\API\Auth;

use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Complete password reset.
     *
     * @param  Request $request
     * @return \App\User
     */
    public function reset(Request $request)
    {
        try {
            $request->validate([
                'password' => 'required|confirmed|min:6',
                'email' => 'required',
            ]);

            $credentials = $request->only(['email', 'password', 'password_confirmation', 'token']);

            $response = Password::broker()->reset(
                $credentials, function ($user, $password) {
                    $user->password = Hash::make($password);
                    $user->save();
                }
            );
            
            PasswordReset::where('email', $request->email)->delete();

            return response()->json(['message' => 'Password reset completed.'], 200);            
        }
        catch (Exception $error) {
            return response()->json([
                'message' => $error,
            ], 401);
        }
    }
}
