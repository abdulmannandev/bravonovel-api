<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NovelRepositories\NovelRepository;
use App\Resources\Novel\NovelResource;

class NovelController extends Controller
{
    /**
     * NovelRepository object
     *
     * @var App\Repositories\NovelRepositories\NovelRepository
     */
    protected $novelRepository;

    /**
     * Create a new controller instance.
     *
     * @param  App\Repositories\NovelRepositories\NovelRepository $novelResource
     * @return void
     */
    public function __construct(NovelRepository $novelRepository)
    {
        $this->novelRepository = $novelRepository;
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return NovelResource::collection($this->novelRepository->getAll(request()->input('genre_id')));
    }

    /**
     * Returns a individual resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return new NovelResource(
            $this->novelRepository->findOrFail($id)
        );
    }
}
