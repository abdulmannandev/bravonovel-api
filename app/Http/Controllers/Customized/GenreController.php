<?php

namespace App\Http\Controllers\Customized;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\GenreRepositories\GenreRepository;

class GenreController extends Controller
{
    private $genreRepository;

    /**
     * CrudController constructor.
     * @param CrudService $service
     * @param CrudFilter $filter
     */
    public function __construct(GenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $query = $this->genreRepository->scopeQuery(function($query) {
            foreach (request()->except(['page', 'per_page', 'pagination', 'orderBy', 'search']) as $input => $value) {
                if (!is_null($value))
                    $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if (request()->get('orderBy'))
                $query = $query->orderBy('id', request()->input('orderBy'));

            if (request()->has('search') && !is_null(request()->input('search')))
                $query = $query->where('title', 'like', '%'.request()->input('search').'%');

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('per_page') ?? 10);
        } else {
            $results = $query->get();
        }

        return $results;
    }

    public function view()
    {
        return view('genre.index');
    }

    public function get($id)
    {
        return $this->genreRepository->findOrFail($id);
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required'
        ]);
        
        $this->genreRepository->create(request()->all());

        return created_responses('data');

    }

    public function update($id)
    {
        $this->validate(request(), [
            'id' => 'required',
            'name' => 'required'
        ]);

        $this->genreRepository->update(request()->all(), $id);

        return updated_responses('data');
    }

    public function destroy($id)
    {
        if ($this->genreRepository->delete($id)) {
            return deleted_responses('data');
        }
        return failed_responses();
    }

}
