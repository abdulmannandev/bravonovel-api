<?php

namespace App\Http\Controllers\Customized;

use App\Http\Controllers\Controller;
use App\Repositories\GiftRepositories\GiftRepository;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class GiftController extends Controller
{
    private $giftRepository;

    /**
     * CrudController constructor.
     * @param CrudService $service
     * @param CrudFilter $filter
     */
    public function __construct(GiftRepository $giftRepository)
    {
        $this->giftRepository = $giftRepository;
    }

    public function view()
    {
        return view('gifts.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = $this->giftRepository->scopeQuery(function($query) {
            foreach (request()->except(['page', 'per_page', 'pagination', 'orderBy', 'search']) as $input => $value) {
                if (!is_null($value))
                    $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if (request()->get('orderBy'))
                $query = $query->orderBy('id', request()->input('orderBy'));

            if (request()->has('search') && !is_null(request()->input('search')))
                $query = $query->where('title', 'like', '%'.request()->input('search').'%');

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('per_page') ?? 10);
        } else {
            $results = $query->get();
        }

        return $results;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title' => 'required',
            'img' => 'required|mimes:jpeg,jpg,png,gif',
            'price' => 'required',
        ]);

        $data = $request->all();

        if ($request->hasFile('img'))
            $data['img'] = $this->handleFileUpload($request->file('img'));

        $this->giftRepository->create($data);

        return created_responses('data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return $this->giftRepository->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if ($request->hasFile('img'))
            $data['img'] = $this->handleFileUpload($request->file('img'));

        $this->giftRepository->update($data, $id);

        return updated_responses('data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->giftRepository->delete($id)) {
            return deleted_responses('data');
        }
        return failed_responses();
    }

    /**
     * @param $file
     * @return string
     */
    private function handleFileUpload($file) {
        $filenameonly = str_replace($file->getClientOriginalExtension(), "", $file->getClientOriginalName());
        $filename = time() . '-' . Str::slug($filenameonly, '-') .'.' . $file->getClientOriginalExtension();
        $path = '/storage/images/';
        $destinationPath = public_path() . $path;
        $file->move($destinationPath, $filename);
        return $path . $filename;
    }
}
