<?php

namespace App\Http\Controllers\Customized;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Repositories\NovelRepositories\NovelRepository;

class NovelController extends Controller
{
    private $novelRepository;

    /**
     * CrudController constructor.
     * @param CrudService $service
     * @param CrudFilter $filter
     */
    public function __construct(NovelRepository $novelRepository)
    {
        $this->novelRepository = $novelRepository;
    }

    public function view()
    {
        return view('novel.index');
    }

    public function get($id)
    {
        return $this->novelRepository->with(['novelGenres' => function($q) {
            $q->select('id', 'name as value');
        }])->findOrFail($id);
    }

    public function update($id)
    {
        $this->validate(request(), [
            'image' => 'image'
        ]);

        if(request()->hasFile('image'))
        {
            request()->request->add(['image' => $this->handleFileUpload(request()->file('image'))]);
        }

        $updatedData = $this->novelRepository->update(request()->all(), $id);

        if (request()->get('genre'))
            $updatedData->novelGenres()->sync(request()->get('genre'));
        
        return updated_responses('data');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $currentUser = request()->user();

        $query = $this->novelRepository->scopeQuery(function($query) use($currentUser) {
            foreach (request()->except(['page', 'per_page', 'pagination', 'orderBy', 'search']) as $input => $value) {
                if (!is_null($value))
                    $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if (!$currentUser->isAdmin())
                $query = $query->where('user_id', $currentUser->id);

            if (request()->get('orderBy'))
                $query = $query->orderBy('id', request()->input('orderBy'));

            if (request()->has('search') && !is_null(request()->input('search')))
                $query = $query->where('title', 'like', '%'.request()->input('search').'%');

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('per_page') ?? 10);
        } else {
            $results = $query->get();
        }

        return $results;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image'
        ]);

        if($request->hasFile('image'))
        {
            $request->request->add(['image' => $this->handleFileUpload($request->file('image'))]);
        }

        $user_id = $request->user()->id;
        $request->request->add(['user_id' => $user_id]);

        $createdData = $this->novelRepository->create($request->all());

        if (request()->get('genre'))
            $createdData->novelGenres()->sync(request()->get('genre'));

        return created_responses('data');
    }

    public function destroy($id)
    {
        if ($this->novelRepository->delete($id)) {
            return deleted_responses('data');
        }
        return failed_responses();
    }

    /**
     * @param $file
     * @return string
     */
    private function handleFileUpload($file) {
        $filenameonly = str_replace($file->getClientOriginalExtension(), "", $file->getClientOriginalName());
        $filename = time() . '-' . Str::slug($filenameonly, '-') .'.' . $file->getClientOriginalExtension();
        $path = '/storage/images/';
        $destinationPath = public_path() . $path;
        $file->move($destinationPath, $filename);
        return $path . $filename;
    }
}
