<?php

namespace App\Http\Controllers\Customized;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NovelChapterRepositories\NovelChapterRepository;

class ChapterNovelController extends Controller
{
    private $novelChapterRepository;

    /**
     * CrudController constructor.
     * @param CrudService $service
     * @param CrudFilter $filter
     */
    public function __construct(NovelChapterRepository $novelChapterRepository)
    {
        $this->novelChapterRepository = $novelChapterRepository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $currentUser = request()->user();

        $query = $this->novelChapterRepository->scopeQuery(function($query) use($currentUser) {
            foreach (request()->except(['page', 'per_page', 'pagination', 'orderBy', 'search']) as $input => $value) {
                if (!is_null($value))
                    $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if (!$currentUser->isAdmin())
                $query = $query->where('user_id', $currentUser->id);

            if (request()->get('orderBy'))
                $query = $query->orderBy('id', request()->input('orderBy'));

            if (request()->has('search') && !is_null(request()->input('search')))
                $query = $query->where('title', 'like', '%'.request()->input('search').'%');

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('per_page') ?? 10);
        } else {
            $results = $query->get();
        }

        return $results;
    }

    public function view()
    {
        return view('novelChapters.index');
    }

    public function get($id)
    {
        return $this->novelChapterRepository->findOrFail($id);
    }

    public function store(Request $request, $id)
    {
        $user_id = $request->user()->id;
        $request->request->add(['user_id' => $user_id, 'novel_id' => $id]);

        $this->novelChapterRepository->create(request()->all());

        return created_responses('data');

    }

    public function update($id)
    {
        $this->novelChapterRepository->update(request()->all(), $id);

        return updated_responses('data');
    }

    public function destroy($id)
    {
        if ($this->novelChapterRepository->delete($id)) {
            return deleted_responses('data');
        }
        return failed_responses();
    }
}
