<?php

namespace App\Http\Controllers\Customized;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NovelReviewRepositories\NovelReviewRepository;

class ReviewController extends Controller
{
    private $novelReviewRepository;

    /**
     * CrudController constructor.
     * @param CrudService $service
     * @param CrudFilter $filter
     */
    public function __construct(NovelReviewRepository $novelReviewRepository)
    {
        $this->novelReviewRepository = $novelReviewRepository;
    }

    public function view()
    {
        return view('novel-review.index');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $query = $this->novelReviewRepository->scopeQuery(function($query) {
            foreach (request()->except(['page', 'per_page', 'pagination', 'orderBy', 'search']) as $input => $value) {
                if (!is_null($value))
                    $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if (request()->get('orderBy'))
                $query = $query->orderBy('id', request()->input('orderBy'));

            if (request()->has('search') && !is_null(request()->input('search')))
                $query = $query->where('title', 'like', '%'.request()->input('search').'%');

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('per_page') ?? 10);
        } else {
            $results = $query->get();
        }

        return $results;
    }
}
