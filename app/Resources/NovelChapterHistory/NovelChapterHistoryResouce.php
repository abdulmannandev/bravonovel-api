<?php

namespace App\Resources\NovelChapterHistory;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Resources\User\UserResource;

class NovelChapterHistoryResouce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'user' => new UserResource($this->user),
            'chapter_id' => $this->chapter_id,
            'watched_at' => $this->watched_at
        ]
    }
}
