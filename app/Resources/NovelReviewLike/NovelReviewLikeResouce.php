<?php

namespace App\Resources\NovelReviewLike;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Resources\User\UserResource;

class NovelReviewLikeResouce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
             'id'=>$this->id,
            'user' => new UserResource($this->user),
            'review_id' => $this->review_id,
            'like_at' => $this->like_at
        ];
    }
}
