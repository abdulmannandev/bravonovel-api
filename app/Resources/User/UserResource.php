<?php

namespace App\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'img' => $this->img,
            'coins' => $this->coins,
            'diamond' => $this->diamond,
            'email' => $this->email,
            'gender' => $this->gender
        ];
    }
}
