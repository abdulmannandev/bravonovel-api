<?php

namespace App\Resources\NovelChapter;

use Illuminate\Http\Resources\Json\JsonResource;

class NovelChapterResouce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'text' => $this->text,
            'order' => $this->order,
            'locked' => $this->locked,
            'watch_count' => $this->watch_count,
            'created_at' => $this->created_at
        ];
    }
}
