<?php

namespace App\Resources\Novel;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Resources\User\UserResource;

class NovelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'keyword' => $this->keyword,
            'synopsis' => $this->synopsis,
            'user' => new UserResource($this->user),
            'image' => $this->image,
            'state' => $this->state,
            'published' => $this->published,
            'watch_count' => $this->watch_count,
            'created_at' => $this->created_at
        ];
    }
}
