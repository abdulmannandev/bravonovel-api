<?php

namespace App\Resources\Gift;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Resources\User\UserResource;

class GiftResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->img,
            'price' => $this->price
        ];
    }
}
