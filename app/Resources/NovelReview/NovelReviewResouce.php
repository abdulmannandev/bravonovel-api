<?php

namespace App\Resources\NovelReview;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Resources\User\UserResource;

class NovelReviewResouce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
             'id'=>$this->id,
            'user' => new UserResource($this->user),
            'text' => $this->text,
            'rate' => $this->rate,
            'like_count' => $this->like_count
        ];
    }
}
