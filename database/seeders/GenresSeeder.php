<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = [
            "Literature",
            "Short Stories",
            "Sci-Fi",
            "Action & Adventure",
            "Mystery",
            "Fantasy",
            "Romance",
            "Horror",
            "Historical",
            "Humorous",
        ];

        foreach ($genres as $genre) {
            DB::table("novel_genres")->insert([
                "name" => $genre
            ]);
        }
        
    }
}
