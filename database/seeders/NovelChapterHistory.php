<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
//use App\Models\NovelChapterHistory;
use Illuminate\Support\Facades\DB;
class NovelChapterHistory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        $chapterHistory = [
           [
                'user_id' => '1',
                'novel_id' => '1',
                'chapter_id' => '1',
                'watched_at' => '2020-12-13 07:18:40'

            ],
            [
                'user_id' => '1',
                'novel_id' => '2',
                'chapter_id' => '1',
                'watched_at' => '2020-12-14 07:18:40'
            ],
            [
                'user_id' => '2',
                'novel_id' => '3',
                'chapter_id' => '3',
                'watched_at' => '2020-12-14 07:19:50'
            ],
        ];
        DB::table("novel_chapter_history")->insert($chapterHistory);
        
    }
}
