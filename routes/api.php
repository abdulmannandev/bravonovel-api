<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\API\Auth\VerificationController;
use App\Http\Controllers\API\Auth\ResetPasswordController;
use App\Http\Controllers\API\Auth\ForgotPasswordController;
use App\Http\Controllers\API\GenreController;
use App\Http\Controllers\API\NovelController;
use App\Http\Controllers\API\NovelChaptersController;
use App\Http\Controllers\API\NovelChaptersHistoryController;
use App\Http\Controllers\API\NovelReviewController;
use App\Http\Controllers\API\NovelReviewLikeController;
use App\Http\Controllers\API\ResourceController;
use App\Http\Controllers\API\GiftController;

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);
Route::get('verify-email', [VerificationController::class, 'verify'])->name('verification.verify');
Route::get('resend-email', [VerificationController::class, 'resend']);
Route::post('reset-password', [ResetPasswordController::class, 'reset'])->name('password.reset');
Route::post('forgot-password', [ForgotPasswordController::class, 'forgot']);
Route::post('change-password', [UserController::class, 'changePassword']);

// genres

Route::get('genres', [ResourceController::class, 'index'])->defaults('_config', [
    "repository" => "App\Repositories\GenreRepositories\GenreRepository",
    'resource' => 'App\Resources\Genre\NovelGenre'
]);

Route::get('genres/{id}', [ResourceController::class, 'get'])->defaults('_config', [
    "repository" => "App\Repositories\GenreRepositories\GenreRepository",
    'resource' => 'App\Resources\Genre\NovelGenre'
]);

// novels
Route::get('novels', [NovelController::class, 'index']);
Route::get('novels/{id}', [NovelController::class, 'get']);

// novel chapter

Route::post('novel-chapters', [NovelChaptersController::class, 'getNovelChapters']);
Route::get('novel-chapters-history', [NovelChaptersHistoryController::class, 'getNovelChaptersHistory']);

//Novel Review
Route::get('novel-review', [NovelReviewController::class, 'getNovelReview']);
Route::post('novel-review', [NovelReviewController::class, 'StoreNovelReview']);

//Novel Review like
Route::get('novel-review-like', [NovelReviewLikeController::class, 'getNovelReviewLike']);
Route::post('novel-review-like', [NovelReviewLikeController::class, 'StoreNovelReviewLike']);

//Gifts
Route::get('gifts', [ResourceController::class, 'index'])->defaults('_config', [
    "repository" => "App\Repositories\GiftRepositories\GiftRepository",
    'resource' => 'App\Resources\Gift\GiftResource'
]);
Route::get('gifts/user', [GiftController::class, 'index']);
Route::post('gifts/user/create', [GiftController::class, 'store']);
Route::get('gifts/{id}', [ResourceController::class, 'get'])->defaults('_config', [
    "repository" => "App\Repositories\GiftRepositories\GiftRepository",
    'resource' => 'App\Resources\Gift\GiftResource'
]);
