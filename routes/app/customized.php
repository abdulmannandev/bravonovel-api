<?php

use App\Http\Controllers\Customized\GenreController;
use App\Http\Controllers\Customized\NovelController;
use App\Http\Controllers\Customized\ChapterNovelController;
use App\Http\Controllers\Customized\ReviewController;
use App\Http\Controllers\Customized\GiftController;

// genres
Route::get('genres', [GenreController::class, 'view']);
Route::put('genres-data/{id}', [GenreController::class, 'update']);
Route::get('genres-data', [GenreController::class, 'index']);
Route::post('genres/create', [GenreController::class, 'store']);
Route::get('genres-data/{id}', [GenreController::class, 'get']);
Route::delete('genres-data/{id}', [GenreController::class, 'destroy']);

//novel
Route::get('novel', [NovelController::class, 'view']);
Route::get('novel-data', [NovelController::class, 'index']);
Route::post('novel-data', [NovelController::class, 'store']);
Route::get('novel-data/{id}', [NovelController::class, 'get']);
Route::patch('novel-data/{id}', [NovelController::class, 'update']);
Route::delete('novel-data/{id}', [NovelController::class, 'destroy']);

//novel chapter
Route::get('novelchapter', [ChapterNovelController::class, 'view']);
Route::get('novelchapter-data', [ChapterNovelController::class, 'index']);
Route::post('novelchapter-data/create/{id}', [ChapterNovelController::class, 'store']);
Route::get('novelchapter-data/{id}', [ChapterNovelController::class, 'get']);
Route::patch('novelchapter-data/{id}', [ChapterNovelController::class, 'update']);
Route::delete('novelchapter-data/{id}', [ChapterNovelController::class, 'destroy']);

//novel reviews
Route::get('novelreview', [ReviewController::class, 'view']);
Route::get('novelreview-data', [ReviewController::class, 'index']);

//gifts
Route::get('gifts', [GiftController::class, 'view']);
Route::get('gifts-data', [GiftController::class, 'index']);
Route::post('gifts-data/create', [GiftController::class, 'store']);
Route::get('gifts-data/{id}', [GiftController::class, 'get']);
Route::patch('gifts-data/{id}', [GiftController::class, 'update']);
Route::delete('gifts-data/{id}', [GiftController::class, 'destroy']);
