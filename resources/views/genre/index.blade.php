@extends('layouts.app')

@section('title', trans('custom.genre'))

@section('contents')
    <genre-view></genre-view>
@endsection
