@extends('layouts.app')

@section('title', trans('custom.novel_review'))

@section('contents')
    <novel-review-view></novel-review-view>
@endsection
