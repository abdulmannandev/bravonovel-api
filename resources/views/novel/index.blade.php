@extends('layouts.app')

@section('title', trans('custom.novel'))

@section('contents')
    <novel-view></novel-view>
@endsection
