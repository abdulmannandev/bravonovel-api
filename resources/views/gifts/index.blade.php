@extends('layouts.app')

@section('title', trans('custom.gifts'))

@section('contents')
    <gift-view></gift-view>
@endsection
