@extends('layouts.app')

@section('title', trans('custom.novel_chapters'))

@section('contents')
    <novel-chapter-view></novel-chapter-view>
@endsection
