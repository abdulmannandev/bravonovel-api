@php
    $data = [
        [
            'id' => 'dashboard-samples',
            'icon' => 'pie-chart',
            'name' => trans('custom.dashboard'),
            'url' => '/admin/dashboard',
            'permission' => authorize_any(['view_default','view_academy','view_ecmommerce','view_hospital','view_hrm']),
        ],
        [
           'icon' => 'grid',
           'name' => trans('custom.genre'),
           'url' => '/genres',
           'permission' => auth()->user()->can('view_genre'),
        ],
        [
            'icon' => 'grid',
            'name' => trans('custom.novel'),
            'url' => '/novel',
            'permission' => auth()->user()->can('novel'),
        ],
        [
            'icon' => 'grid',
            'name' => trans('custom.novel_chapters'),
            'url' => '/novelchapter',
            'permission' => auth()->user()->can('view_chapters'),
        ],
        [
            'icon' => 'grid',
            'name' => trans('custom.novel_review'),
            'url' => '/novelreview',
            'permission' => auth()->user()->can('novel_review'),
        ],
        [
            'icon' => 'grid',
            'name' => trans('custom.gifts'),
            'url' => '/gifts',
            'permission' => auth()->user()->can('gifts'),
        ],
        [
           'icon' => 'user-check',
           'name' => trans('custom.user_and_roles'),
           'url' => '/users-and-roles',
           'permission' => authorize_any(['view_users', 'view_roles', 'invite_user', 'create_roles']),
        ],
        [
           'icon' => 'settings',
           'name' => trans('custom.settings'),
           'url' => '/app-setting',
           'permission' => authorize_any(
           [
                'view_settings', 'update_settings', 'view_delivery_settings',
                'update_delivery_settings', 'view_sms_settings', 'update_sms_settings', 'view_recaptcha_settings',
                'view_notification_settings', 'update_notification_settings', 'update_notification_templates',
                'view_notification_templates'
           ])
        ],
    ];
@endphp

<dashboard-sidebar :data="{{ json_encode($data) }}"
                   :logo="{{json_encode(config('settings.application.company_logo'))}}"
                   :logo-icon="{{json_encode(config('settings.application.company_icon'))}}">
</dashboard-sidebar>
