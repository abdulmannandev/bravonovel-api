export default {
    state: {
        loggedInUser: {},
        settings: {
            dateFormat: "YYYY-MM-DD",
            timeFormat: 12
        },
        theme: {
            darkMode: false
        }
    }

    // getters: {
    // },

    // actions: {

    // },

    // mutations: {

    // }
}
