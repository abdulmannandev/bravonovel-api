// genre
export const GENRE_DATA = '/genres-data';

// novel
export const NOVEL_DATA = '/novel-data';
export const NOVEL_DATA_UPDATE = '/novel-data/update';

// novel chapters
export const NOVEL_CHAPTER = '/novelchapter-data';

// novel review
export const NOVEL_REVIEW = 'novelreview-data';

// gifts
export const GIFT_DATA = 'gifts-data';
