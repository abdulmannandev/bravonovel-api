export const TableHelpers = {
    data() {
        return {
            tableColumns : [
                {
                    title: this.$t('id'),
                    type: 'text',
                    key: 'id',
            
                },
                {
                    title: this.$t('name'),
                    type: 'text',
                    key: 'name',
            
                },
            ],
            actionObj : {
                title: this.$t('action'),
                type: 'action',
            }
        }
    },
}
