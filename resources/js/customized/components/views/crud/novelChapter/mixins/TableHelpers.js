export const TableHelpers = {
    data() {
        return {
            tableColumns : [
                {
                    title: this.$t('id'),
                    type: 'text',
                    key: 'id',
            
                },
                {
                    title: this.$t('title'),
                    type: 'text',
                    key: 'title',
            
                },
                {
                    title: this.$t('text'),
                    type: 'text',
                    key: 'text',
            
                },
                {
                    title: this.$t('order'),
                    type: 'text',
                    key: 'order',
            
                },
                {
                    title: this.$t('locked'),
                    type: 'custom-html',
                    key: 'locked',
                    modifier: (value) => {
                        if (value) {
                            let ClassName = 'danger';

                            if (value === 'yes') ClassName = `success`;
                            else if (value === 'no') ClassName = `danger`;

                            return `<span class="badge badge-sm badge-pill badge-${ClassName}">${this.$t(value)}</span>`;
                        }
                    }
            
                },
                {
                    title: this.$t('watch_count'),
                    type: 'text',
                    key: 'watch_count',
            
                }
            ],
            actionObj : {
                title: this.$t('action'),
                type: 'action',
            }
        }
    },
}
