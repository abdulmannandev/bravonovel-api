export const TableHelpers = {
    data() {
        return {
            tableColumns : [
                {
                    title: this.$t('id'),
                    type: 'text',
                    key: 'id',
            
                },
                {
                    title: this.$t('text'),
                    type: 'text',
                    key: 'text',
            
                },
                {
                    title: this.$t('rate'),
                    type: 'text',
                    key: 'rate',
            
                },
                {
                    title: this.$t('like_count'),
                    type: 'text',
                    key: 'like_count',
            
                }
            ],
            actionObj : {
                title: this.$t('action'),
                type: 'action',
            }
        }
    },
}
