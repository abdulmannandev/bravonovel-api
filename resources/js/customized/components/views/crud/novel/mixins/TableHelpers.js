export const TableHelpers = {
    data() {
        return {
            tableColumns : [
                {
                    title: this.$t('id'),
                    type: 'text',
                    key: 'id',
                },
                {
                    title: this.$t('title'),
                    type: 'text',
                    key: 'title',
                },
                {
                    title: this.$t('synopsis'),
                    type: 'text',
                    key: 'synopsis',
                },
                {
                    title: this.$t('state'),
                    type: 'custom-html',
                    key: 'state',
                    isVisible: true,
                    modifier: (value) => {
                        if (value) {
                            let ClassName = 'info';

                            if (value === 'completed') ClassName = `success`;
                            else if (value === 'ongoing') ClassName = `info`;

                            return `<span class="badge badge-sm badge-pill badge-${ClassName}">${this.$t(value)}</span>`;
                        }
                    }
                },
                {
                    title: this.$t('published'),
                    type: 'custom-html',
                    key: 'published',
                    isVisible: true,
                    modifier: (value) => {
                        if (value) {
                            let ClassName = 'danger';

                            if (value === 'yes') ClassName = `success`;
                            else if (value === 'no') ClassName = `danger`;

                            return `<span class="badge badge-sm badge-pill badge-${ClassName}">${this.$t(value)}</span>`;
                        }
                    }
                },
                {
                    title: this.$t('watch_count'),
                    type: 'text',
                    key: 'watch_count',
                },
                {
                    title: this.$t('keyword'),
                    type: 'text',
                    key: 'keyword',
                },
            ],
            actionObj : {
                title: this.$t('action'),
                type: 'action',
            }
        }
    },
}
