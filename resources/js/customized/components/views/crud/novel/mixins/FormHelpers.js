export const FormHelpers = {
    props: {
        genreUrl: {
            type: String,
            default: null,
        },
    },
    created() {
        if (this.genreUrl) {
            this.getGenreData(this.genreUrl);
        }
    },
    methods: {
        getGenreData(url) {
            if (this.isFunction(this.beforeGetGenreData))
                this.beforeGetGenreData();

            this.axiosGet(url)
                .then((response) => {
                    if (this.isFunction(this.afterSuccessFromGetGenreData))
                        this.afterSuccessFromGetGenreData(response);

                }).catch(({response}) => {

                if (this.isFunction(this.afterErrorFromGetGenreData))
                    this.afterErrorFromGetGenreData(response);
            });
        }
    }
}
