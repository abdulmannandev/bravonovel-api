export const TableHelpers = {
    data() {
        return {
            tableColumns : [
                {
                    title: this.$t('id'),
                    type: 'text',
                    key: 'id',
            
                },
                {
                    title: this.$t('title'),
                    type: 'text',
                    key: 'title',
            
                },
                {
                    title: this.$t('price'),
                    type: 'text',
                    key: 'price',
            
                },
                {
                    title: this.$t('img'),
                    type: 'img',
                    key: 'img',
            
                },
            ],
            actionObj : {
                title: this.$t('action'),
                type: 'action',
            }
        }
    },
}
