import Vue from 'vue';
window.moment = require('moment');

// genre
Vue.component('genre-view', require('./components/views/crud/genre/index').default);
Vue.component('genre-add-model', require('./components/views/crud/genre/GenreAddModel').default);

// novel
Vue.component('novel-view', require('./components/views/crud/novel/index').default);
Vue.component('novel-add-modal', require('./components/views/crud/novel/NovelAddModal').default);

// novel chapters
Vue.component('novel-chapter-view', require('./components/views/crud/novelChapter/index').default);
Vue.component('chapter-add-modal', require('./components/views/crud/novelChapter/ChapterAddModal').default);

// novel reviews
Vue.component('novel-review-view', require('./components/views/crud/novelReview/index').default);

// Gift
Vue.component('gift-view', require('./components/views/crud/gift/index').default);
Vue.component('gift-add-model', require('./components/views/crud/gift/GiftAddModel').default);
